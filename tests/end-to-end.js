var Core =  require("../core");

Core.middleware.push(require("../middleware/prefix"));
Core.middleware.push(require("../middleware/passthrough"));

const console = require('../logger')();
console.middleware.push(function(next,options,message) {
  next(options,'PR: '+message);
})

console.info("This is an info");
console.log("This is a log");
console.warn("This is a warn");
console.error("This is an error");

