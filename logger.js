var Core = require('./core');

class Logger {
  constructor(options) {
    this.options = options;
    this.middleware = [];
  }

  extend(options) {
    var logger = new Logger(Core.extend(this.options,options));
    logger.middleware = this.middleware.slice();
    return logger;
  }
  
  log() {
    Core.log.apply(Core,[Core.extend(this.options, { middleware: this.middleware, level: 'log' })].concat(Array.prototype.slice.call(arguments)));
  }

  info() {
    Core.log.apply(Core,[Core.extend(this.options, { middleware: this.middleware, level: 'info' })].concat(Array.prototype.slice.call(arguments)));
  }
  
  warn() {
    Core.log.apply(Core,[Core.extend(this.options, { middleware: this.middleware, level: 'warn' })].concat(Array.prototype.slice.call(arguments)));
  }

  error() {
    Core.log.apply(Core,[Core.extend(this.options, { middleware: this.middleware, level: 'error' })].concat(Array.prototype.slice.call(arguments)));
  }
}

module.exports = function(options) {
  return new Logger(options);
}
